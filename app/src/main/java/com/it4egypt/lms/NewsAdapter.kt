package com.it4egypt.lms

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.it4egypt.lms.models.NewsModel

class NewsAdapter(val context: Context, val news:ArrayList<NewsModel>, val listItemClickListener: ListItemClickListener): RecyclerView.Adapter<NewsAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val itemView: View = LayoutInflater.from(context).inflate(R.layout.news_list_item, parent, false)
        return ViewHolder(itemView)
    }

    override fun getItemCount(): Int {
        return news.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.title.text = news[position].title
        news[position].validFromDate?.let {
            holder.body.text = it.subSequence(0,10)
        }
        holder.itemView.setOnClickListener {
            listItemClickListener.onItemClick(news[position])
        }
    }

    class ViewHolder(itemView:View) : RecyclerView.ViewHolder(itemView) {
        var title:TextView = itemView.findViewById(R.id.title)
        var body:TextView = itemView.findViewById(R.id.body)
    }

    interface ListItemClickListener{
        fun onItemClick(news: NewsModel)
    }
}