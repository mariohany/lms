package com.it4egypt.lms

import com.it4egypt.lms.models.ClassRoomModel

interface RoomItemClick {
    fun onCLick(room: ClassRoomModel)
}