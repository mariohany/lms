package com.it4egypt.lms.retrofit

import com.it4egypt.lms.models.*
import retrofit2.Call
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.POST
import retrofit2.http.Query


interface APIInterface {

    @POST("api/School")
    fun GetSchoolConfig(
            @Body schoolId:String
    ): Call<ArrayList<School>>

    @POST("api/Login")
    fun Login(
            @Query("username") username:String,
            @Body password:String
    ): Call<LoginResponse>

    @POST("api/FirebaseToken")
    fun FirebaseToken(
            @Query("id") userId:Int,
            @Body data: DeviceData
    ): Call<String>

    @GET("api/VirtualClassLink")
    fun VirtualClassRoom(
            @Query("userToken") token:String
    ): Call<ClassRoomResponse>

    @GET("api/Menu")
    fun Menu(
            @Query("userToken") token:String
    ): Call<ArrayList<MenuItem>>

    @GET("api/News")
    fun News(
            @Query("userToken") token:String,
            @Query("isArabic") isArabic:Boolean
    ): Call<ArrayList<NewsModel>>

    @GET("api/ActiveTasksQuizCounts")
    fun Counters(
            @Query("userToken") token:String
    ): Call<CountersResponse>

    @GET("api/MobileAppVer")
    fun CheckVersion(
        @Query("isAndroid") isAndroid: Boolean
    ): Call<Int>
}