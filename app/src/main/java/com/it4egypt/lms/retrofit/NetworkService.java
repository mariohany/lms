package com.it4egypt.lms.retrofit;

import android.content.Context;

import com.it4egypt.lms.models.ClassRoomResponse;
import com.it4egypt.lms.models.CountersResponse;
import com.it4egypt.lms.models.DeviceData;
import com.it4egypt.lms.models.LoginResponse;
import com.it4egypt.lms.models.MenuItem;
import com.it4egypt.lms.models.NewsModel;
import com.it4egypt.lms.models.School;

import java.util.ArrayList;

import retrofit2.Call;

public class NetworkService {
    private static NetworkService INSTANCE = null;

    private NetworkService(){}

    public static NetworkService getInstance() {
        if (INSTANCE == null)
            INSTANCE = new NetworkService();
        return INSTANCE;
    }

    public void GetSchoolConfig(BaseResponse.BaseResponseListener<ArrayList<School>> listener, Context context, String schoolId){
        RestClient restClient = new RestClient();
        APIInterface apiInterface = restClient.createAdminService();
        Call<ArrayList<School>> call = apiInterface.GetSchoolConfig(schoolId);
        call.enqueue(new BaseResponse<>(listener, context));
    }

    public void Login(BaseResponse.BaseResponseListener<LoginResponse> listener, Context context, String userName, String password){
        RestClient restClient = new RestClient();
        APIInterface apiInterface = restClient.createService(context);
        Call<LoginResponse> call = apiInterface.Login(userName, password);
        call.enqueue(new BaseResponse<>(listener, context));
    }

    public void FirebaseToken(BaseResponse.BaseResponseListener<String> listener, Context context, int userId, DeviceData data){
        RestClient restClient = new RestClient();
        APIInterface apiInterface = restClient.createService(context);
        Call<String> call = apiInterface.FirebaseToken(userId, data);
        call.enqueue(new BaseResponse<>(listener, context));
    }

    public void VirtualClassRoom(BaseResponse.BaseResponseListener<ClassRoomResponse> listener, Context context, String token){
        RestClient restClient = new RestClient();
        APIInterface apiInterface = restClient.createService(context);
        Call<ClassRoomResponse> call = apiInterface.VirtualClassRoom(token);
        call.enqueue(new BaseResponse<>(listener, context));
    }

    public void Menu(BaseResponse.BaseResponseListener<ArrayList<MenuItem>> listener, Context context, String token){
        RestClient restClient = new RestClient();
        APIInterface apiInterface = restClient.createService(context);
        Call<ArrayList<MenuItem>> call = apiInterface.Menu(token);
        call.enqueue(new BaseResponse<>(listener, context));
    }

    public void News(BaseResponse.BaseResponseListener<ArrayList<NewsModel>> listener, Context context, String token, boolean isArabic){
        RestClient restClient = new RestClient();
        APIInterface apiInterface = restClient.createService(context);
        Call<ArrayList<NewsModel>> call = apiInterface.News(token,  isArabic);
        call.enqueue(new BaseResponse<>(listener, context));
    }

    public void Counters(BaseResponse.BaseResponseListener<CountersResponse> listener, Context context, String token){
        RestClient restClient = new RestClient();
        APIInterface apiInterface = restClient.createService(context);
        Call<CountersResponse> call = apiInterface.Counters(token);
        call.enqueue(new BaseResponse<>(listener, context));
    }

    public void CheckVersion(BaseResponse.BaseResponseListener<Integer> listener, Context context){
        RestClient restClient = new RestClient();
        APIInterface apiInterface = restClient.createAdminService();
        Call<Integer> call = apiInterface.CheckVersion(true);
        call.enqueue(new BaseResponse<>(listener, context));
    }
}
