package com.it4egypt.lms.retrofit;

import android.content.Context;

import com.it4egypt.lms.R;

import java.io.IOException;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class BaseResponse<T> implements Callback<T> {
    private BaseResponseListener<T> listener ;
    private Context context;

    public BaseResponse(BaseResponseListener<T> listener, Context context) {
        this.listener = listener;
        this.context = context;
    }

    @Override
    public void onResponse(Call<T> call, Response<T> response) {
        if(response.isSuccessful() && response.code() == 200) {
            listener.onSuccess(response.body());
        }else if(response.code() == 404){
            listener.onFailed(context.getResources().getString(R.string.call_admin));
        }else{
            try {
                listener.onFailed(response.errorBody().string());
            } catch (IOException e) {
                listener.onFailed("Invalid Data");
            }
        }
    }

    @Override
    public void onFailure(Call<T> call, Throwable t) {
        listener.onFailed(t.getLocalizedMessage());
    }

    public interface BaseResponseListener<T>{
        void onFailed(String errorMessage);
        void onSuccess(T body);
    }
}
