package com.it4egypt.lms.retrofit;

import android.content.Context;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.it4egypt.lms.SharedPref;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class RestClient {

    private static final String BASE_ADMIN_URL = "http://lms-admin-api.it4egypt.com/";

    public static Interceptor interceptor = new Interceptor() {
        @Override
        public Response intercept(Chain chain) throws IOException {
            Request original = chain.request();
            Request.Builder requestBuilder = original.newBuilder();

            requestBuilder.addHeader("It4egyptKey", "ERP:It4egypt");
            requestBuilder.addHeader("appId", "ERP");
            requestBuilder.addHeader("appKey", "It4egypt");

            Request request = requestBuilder
                    .method(original.method(), original.body())
                    .build();
            return chain.proceed(request);
        }
    };


    private static OkHttpClient httpClient = new OkHttpClient.Builder()
            .addInterceptor(interceptor)
            .addInterceptor(new HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.BODY))
            .writeTimeout(60, TimeUnit.SECONDS)
            .readTimeout(60, TimeUnit.SECONDS)
            .connectTimeout(60, TimeUnit.SECONDS)
            .build();

    private static Gson gson = new GsonBuilder()
            .setLenient()
            .create();

    public static Retrofit adminRetrofit = new Retrofit.Builder()
            .baseUrl(BASE_ADMIN_URL)
            .client(httpClient)
            .addConverterFactory(GsonConverterFactory.create(gson))
            .build();

    public APIInterface createAdminService() {
        return adminRetrofit.create(APIInterface.class);
    }

    public APIInterface createService(Context context) {
        Retrofit.Builder retrofit = new Retrofit.Builder()
                .baseUrl(SharedPref.INSTANCE.getAPIUrl(context).replace(" ",""))
                .client(httpClient)
                .addConverterFactory(GsonConverterFactory.create(gson));
        return retrofit.build().create(APIInterface.class);
    }

}
