package com.it4egypt.lms

import android.app.Application
import android.app.NotificationChannel
import android.app.NotificationManager
import android.content.Context
import android.os.Build

class LMSApplication: Application() {
    companion object {
        var context:Context?= null
        var url: String? = null
        var notifiManager:NotificationManager ?= null
        var channel:NotificationChannel ?= null
    }

    override fun onCreate() {
        context = this
        super.onCreate()
        notifiManager = getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager

        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.O){
            channel = NotificationChannel(resources.getString(R.string.fcm_channel_id), "All Furn Notifications", NotificationManager.IMPORTANCE_HIGH)
            channel?.setShowBadge(true)
            channel?.enableLights(true)
            channel?.enableVibration(true)
            notifiManager?.createNotificationChannel(channel!!)
        }
    }

}