package com.it4egypt.lms.services

import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.graphics.BitmapFactory
import android.os.Build
import android.util.Log.e
import androidx.core.app.NotificationCompat
import com.google.firebase.messaging.FirebaseMessagingService
import com.google.firebase.messaging.RemoteMessage
import com.it4egypt.lms.LMSApplication
import com.it4egypt.lms.R
import com.it4egypt.lms.SharedPref
import com.it4egypt.lms.ui.activity.HomeActivity
import java.util.*

class FCMService  : FirebaseMessagingService() {

    override fun onNewToken(token: String) {
        super.onNewToken(token)
        e("FCM token", token)
        SharedPref.setFCMToken(applicationContext,token)
    }

    override fun onMessageReceived(remoteMessage: RemoteMessage) {
        var intent = Intent()
        intent = Intent(this, HomeActivity::class.java)
        if (remoteMessage.data.toMap().containsKey("redirectUrl")) {
            if(!remoteMessage.data.toMap()["redirectUrl"].isNullOrEmpty()) {
                LMSApplication.url = remoteMessage.data.toMap()["redirectUrl"].toString()
            }
        }

        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
        val pendingIntent = PendingIntent.getActivity(applicationContext, 0, intent, PendingIntent.FLAG_ONE_SHOT)

        val notifiBuilder = NotificationCompat.Builder(applicationContext, resources.getString(R.string.fcm_channel_id))
                .setSmallIcon(R.drawable.logo)
                .setLargeIcon(BitmapFactory.decodeResource(this.resources, R.drawable.logo))
                .setContentTitle(remoteMessage.notification?.title)
                .setContentText(remoteMessage.notification?.body)
                .setPriority(NotificationCompat.PRIORITY_MAX)
                .setAutoCancel(true)
                .setContentIntent(pendingIntent)

        val notifiManager = getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager

        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.O){
            val channel = NotificationChannel(resources.getString(R.string.fcm_channel_id), "All LMS Notification", NotificationManager.IMPORTANCE_HIGH)
            channel.setShowBadge(true)
            channel.enableLights(true)
            channel.enableVibration(true)
            notifiManager.createNotificationChannel(channel)
        }

        val randomId = Random().nextInt(100) + 65
        notifiManager.notify(randomId, notifiBuilder.build())
    }
}
