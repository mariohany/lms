package com.it4egypt.lms.ui.activity;

import android.Manifest;
import android.animation.ValueAnimator;
import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.DownloadManager;
import android.content.ActivityNotFoundException;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.webkit.CookieManager;
import android.webkit.JsResult;
import android.webkit.PermissionRequest;
import android.webkit.URLUtil;
import android.webkit.ValueCallback;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.widget.Toolbar;
import androidx.cardview.widget.CardView;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.navigation.ui.AppBarConfiguration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.CustomTarget;
import com.bumptech.glide.request.target.Target;
import com.bumptech.glide.request.transition.Transition;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.android.material.navigation.NavigationView;
import com.google.firebase.crashlytics.FirebaseCrashlytics;
import com.hanks.htextview.typer.TyperTextView;
import com.it4egypt.lms.BuildConfig;
import com.it4egypt.lms.ClassRoomAdapter;
import com.it4egypt.lms.LMSApplication;
import com.it4egypt.lms.NewsAdapter;
import com.it4egypt.lms.R;
import com.it4egypt.lms.RoomItemClick;
import com.it4egypt.lms.SharedPref;
import com.it4egypt.lms.models.ClassRoomModel;
import com.it4egypt.lms.models.ClassRoomResponse;
import com.it4egypt.lms.models.CountersResponse;
import com.it4egypt.lms.models.NewsModel;
import com.it4egypt.lms.retrofit.BaseResponse;
import com.it4egypt.lms.retrofit.NetworkService;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.Collections;

public class HomeActivity extends BaseActivity implements BottomNavigationView.OnNavigationItemSelectedListener, BottomNavigationView.OnNavigationItemReselectedListener, NavigationView.OnNavigationItemSelectedListener, RoomItemClick, NewsAdapter.ListItemClickListener {
    private static final String DESKTOP_USER_AGENT = "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.88 Safari/537.36";
    private static final int MY_PERMISSIONS_REQUEST_RECORD_AUDIO = 101;
    private AppBarConfiguration mAppBarConfiguration;
    private static final int MY_PERMISSIONS_REQUEST_CAMERA = 102;
    public static final int REQUEST_SELECT_FILE = 100;
    private final static int FILECHOOSER_RESULTCODE = 1;
    public ValueCallback<Uri[]> uploadMessage;
    private ValueCallback<Uri> mUploadMessage;
    BottomNavigationView bottomNavigationView;
    ArrayList<NewsModel> news = new ArrayList<>();
    ArrayList<ClassRoomModel> rooms = new ArrayList<>();
    ClassRoomAdapter classRoomAdapter;
    RecyclerView roomsRecyclerView;
    WebView webView;
    EditText urlEdit;
    Button btn;
    BroadcastReceiver onComplete;
    TextView homeworkCount, lesionsCount, onlineCount, handCount, noDataView;
    TyperTextView homeworkText, lesionsText, onlineText, handText;
    CardView homeworkCard, lesionsCard, onlineCard, handCard;
    LinearLayout webviewLayout, urlLinearLayout;
    ScrollView homeLayout;
    private PermissionRequest myRequest;
    private MenuItem navLanguage, bottomSchedule, navHome, navMyTasks, navQuizOnline, navSchedule, navQuizHand, navClassRoom, navSemesterDegree, navSendAssignment,
            navSetting, navAbout, bottomMyTasks, bottomClassRoom, bottomHome;
    private RecyclerView newsRecycler;
    private NewsAdapter adapter;
    private DrawerLayout drawer;
    private String downloadUrl;
    private String downloadUserAgent;
    private String downloadContentDisposition;
    private String downloadMimeType;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        checkPermission();
        webView = findViewById(R.id.webview);
//        webView.clearCache(true);
//        webView.clearFormData();
//        webView.clearHistory();
//        webView.clearMatches();
//        webView.clearSslPreferences();
        urlEdit = findViewById(R.id.url_edit);
        urlLinearLayout = findViewById(R.id.linearLayout);
        btn = findViewById(R.id.go_btn);
        noDataView = findViewById(R.id.no_data);
        roomsRecyclerView = findViewById(R.id.rooms_list);
        roomsRecyclerView.setLayoutManager(new LinearLayoutManager(this, RecyclerView.VERTICAL, false));
        classRoomAdapter = new ClassRoomAdapter(this, rooms, this);
        roomsRecyclerView.setAdapter(classRoomAdapter);
        homeworkCount = findViewById(R.id.homework_count);
        lesionsCount = findViewById(R.id.lesions_count);
        onlineCount = findViewById(R.id.online_count);
        handCount = findViewById(R.id.hand_count);
        homeworkText = findViewById(R.id.homework_text);
        lesionsText = findViewById(R.id.lesions_text);
        onlineText = findViewById(R.id.online_text);
        handText = findViewById(R.id.hand_text);
        homeworkCard = findViewById(R.id.homework_card);
        lesionsCard = findViewById(R.id.lesions_card);
        onlineCard = findViewById(R.id.online_card);
        handCard = findViewById(R.id.hand_card);
        homeLayout = findViewById(R.id.home_layout);
        webviewLayout = findViewById(R.id.webview_layout);
        newsRecycler = findViewById(R.id.news_list);
        newsRecycler.setLayoutManager(new LinearLayoutManager(this, RecyclerView.VERTICAL, false));
        adapter = new NewsAdapter(this, news, this);
        newsRecycler.setAdapter(adapter);
//        getWindow().setFlags(WindowManager.LayoutParams.FLAG_SECURE, WindowManager.LayoutParams.FLAG_SECURE);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        setWebViewSetting();
        NavigationView navigationView = findViewById(R.id.nav_view);
        populateMenu(navigationView);
        TextView versionTxt = navigationView.getHeaderView(0).findViewById(R.id.version);
        TextView nameTxt = navigationView.getHeaderView(0).findViewById(R.id.name);
        nameTxt.setText(SharedPref.INSTANCE.getUserName(this));
        ImageView logo = navigationView.getHeaderView(0).findViewById(R.id.logo);
        if (SharedPref.INSTANCE.getUserType(this).equalsIgnoreCase("student")){
            logo.setImageResource(R.drawable.logo_student_white);
        }else{
            logo.setImageResource(R.drawable.logo_white);
        }
        versionTxt.setText("v" + BuildConfig.VERSION_CODE);
        navigationView.setItemTextAppearance(R.style.RegularTextViewStyle);
        navigationView.setNavigationItemSelectedListener(this);
        bottomNavigationView = findViewById(R.id.bottom_nav_view);
        bottomNavigationView.setOnNavigationItemSelectedListener(this);
        bottomNavigationView.setOnNavigationItemReselectedListener(this);
        drawer = findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this, drawer, toolbar,
                R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();
        if (LMSApplication.Companion.getUrl() != null) {
            homeLayout.setVisibility(View.GONE);
            webviewLayout.setVisibility(View.VISIBLE);
            urlLinearLayout.setVisibility(View.GONE);
            roomsRecyclerView.setVisibility(View.GONE);
            noDataView.setVisibility(View.GONE);
            webView.loadUrl(buildUrl(LMSApplication.Companion.getUrl()));
            LMSApplication.Companion.setUrl(null);
        }

        btn.setOnClickListener(view -> webView.loadUrl(urlEdit.getText().toString()));

        navLanguage = navigationView.getMenu().findItem(R.id.nav_language);
        navHome = navigationView.getMenu().findItem(R.id.nav_home);
        navMyTasks = navigationView.getMenu().findItem(R.id.nav_tasks);
        navSchedule = navigationView.getMenu().findItem(R.id.nav_schedule);
        navClassRoom = navigationView.getMenu().findItem(R.id.nav_class_room);
        navSetting = navigationView.getMenu().findItem(R.id.nav_setting);
        navAbout = navigationView.getMenu().findItem(R.id.nav_about);
        bottomMyTasks = bottomNavigationView.getMenu().findItem(R.id.nav_tasks);
        bottomClassRoom = bottomNavigationView.getMenu().findItem(R.id.nav_class_room);
        bottomSchedule = bottomNavigationView.getMenu().findItem(R.id.nav_schedule);
        bottomHome = bottomNavigationView.getMenu().findItem(R.id.nav_home);
    }

    private void populateMenu(NavigationView navigationView) {
        showLoader();
        NetworkService.getInstance().Menu(new BaseResponse.BaseResponseListener<ArrayList<com.it4egypt.lms.models.MenuItem>>() {
            @Override
            public void onFailed(String errorMessage) {
                hideLoader();
            }

            @Override
            public void onSuccess(ArrayList<com.it4egypt.lms.models.MenuItem> body) {
                for (int i=0, j=3; i<body.size();i++,j++){
                    MenuItem item = navigationView.getMenu().add(Menu.NONE, i, j, SharedPref.INSTANCE.getPrefLanguage(HomeActivity.this).equalsIgnoreCase("en")? body.get(i).getEnglish() :body.get(i).getArabic());
                    int finalI = i;
                    Glide.with(HomeActivity.this)
                            .asBitmap()
                            .load(body.get(i).getIcon())
                            .listener(new RequestListener<Bitmap>() {
                                @Override
                                public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Bitmap> target, boolean isFirstResource) {
                                    item.setIcon(R.drawable.ic_homework);
                                    return false;
                                }

                                @Override
                                public boolean onResourceReady(Bitmap resource, Object model, Target<Bitmap> target, DataSource dataSource, boolean isFirstResource) {
                                    item.setIcon(new BitmapDrawable(getResources(), resource));
                                    return false;
                                }
                            })
                            .into(new CustomTarget<Bitmap>() {
                                @Override
                                public void onResourceReady(@NonNull Bitmap resource, @Nullable Transition<? super Bitmap> transition) {

                                }

                                @Override
                                public void onLoadCleared(@Nullable Drawable placeholder) {
                                }
                            });

                    item.setOnMenuItemClickListener(item1 -> {
                        homeLayout.setVisibility(View.GONE);
                        webviewLayout.setVisibility(View.VISIBLE);
                        urlLinearLayout.setVisibility(View.GONE);
                        roomsRecyclerView.setVisibility(View.GONE);
                        noDataView.setVisibility(View.GONE);
                        webView.loadUrl(buildUrl(body.get(finalI).getNavigateUrl()));
                        drawer.close();
                        return true;
                    });
                }
                hideLoader();
            }
        }, this, SharedPref.INSTANCE.getUserToken(this));
    }

    @Override
    protected void onStart() {
        super.onStart();
        loadNewsAndCounters();
    }

    void loadNewsAndCounters(){
        NetworkService.getInstance().News(new BaseResponse.BaseResponseListener<ArrayList<NewsModel>>() {
            @Override
            public void onFailed(String errorMessage) { }

            @Override
            public void onSuccess(ArrayList<NewsModel> body) {
                news.clear();
                news.addAll(body);
                Collections.sort(news, (o1, o2) -> {
                    if(o1.getValidFromDate() != null && o2.getValidFromDate() != null) {
                        if(o1.getValidFromDate().compareTo(o2.getValidFromDate()) > 0)
                            return -1;
                        else if (o1.getValidFromDate().compareTo(o2.getValidFromDate()) < 0)
                            return 1;
                        else
                            return 0;
                    }else
                        return 0;
                });
                adapter.notifyDataSetChanged();
            }
        }, this, SharedPref.INSTANCE.getUserToken(this), SharedPref.INSTANCE.getPrefLanguage(this).equalsIgnoreCase("ar"));
        NetworkService.getInstance().Counters(new BaseResponse.BaseResponseListener<CountersResponse>() {
            @Override
            public void onFailed(String errorMessage) { }

            @Override
            public void onSuccess(CountersResponse body) {
                animateCounter(onlineCount, body.getOnlineQuiz());
                animateCounter(handCount, body.getHandonQuiz());
                animateCounter(homeworkCount, body.getHomework());
                animateCounter(lesionsCount, body.getLessons());
                homeworkText.animateText(getString(R.string.unopened_homework));
                lesionsText.animateText(getString(R.string.unopened_lesions));
                onlineText.animateText(getString(R.string.unopened_online_exams));
                handText.animateText(getString(R.string.unopened_hand_exams));
            }
        }, this, SharedPref.INSTANCE.getUserToken(this));
    }

    public static boolean hasPermissions(Context context, String... permissions) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && context != null && permissions != null) {
            for (String permission : permissions) {
                if (ActivityCompat.checkSelfPermission(context, permission) != PackageManager.PERMISSION_GRANTED) {
                    return false;
                }
            }
        }
        return true;
    }

    @SuppressLint("SetJavaScriptEnabled")
    private void setWebViewSetting() {
        webView.setWebChromeClient(new WebChromeClient() {

            @Override
            public boolean onJsConfirm(WebView view, String url, String message, JsResult result) {
                new AlertDialog.Builder(HomeActivity.this, R.style.DialogeTheme)
                        .setTitle(R.string.app_name)
                        .setMessage(message)
                        .setCancelable(false)
                        .setPositiveButton(android.R.string.ok,
                                (dialog, which) -> result.confirm())
                        .setNegativeButton(android.R.string.cancel,
                                (dialog, which) -> result.cancel())
                        .create()
                        .show();

                return true;
            }

            @Override
            public boolean onJsAlert(WebView view, String url, String message, JsResult result) {
                new AlertDialog.Builder(HomeActivity.this, R.style.DialogeTheme)
                        .setTitle(R.string.app_name)
                        .setCancelable(false)
                        .setMessage(message)
                        .setPositiveButton(R.string.close,
                                (dialog, which) -> result.confirm())
//                        .setNegativeButton(android.R.string.cancel,
//                                (dialog, which) -> result.cancel())
                        .create()
                        .show();

                return true;
            }

            protected void openFileChooser(ValueCallback uploadMsg) {
                mUploadMessage = uploadMsg;
                Intent i = new Intent(Intent.ACTION_GET_CONTENT);
                i.addCategory(Intent.CATEGORY_OPENABLE);
                i.setType("*/*");
                startActivityForResult(Intent.createChooser(i, "File Browser"), REQUEST_SELECT_FILE);
            }

            // For Lollipop 5.0+ Devices
            @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
            public boolean onShowFileChooser(WebView mWebView, ValueCallback<Uri[]> filePathCallback, WebChromeClient.FileChooserParams fileChooserParams)
            {
                if (uploadMessage != null) {
                    uploadMessage.onReceiveValue(null);
                    uploadMessage = null;
                }

                uploadMessage = filePathCallback;
                Intent intent = fileChooserParams.createIntent();
                intent.addCategory(Intent.CATEGORY_OPENABLE);
                intent.setType("*/*");
                try
                {
//                    openFileChooser(uploadMessage);
                    startActivityForResult(intent, REQUEST_SELECT_FILE);
                } catch (ActivityNotFoundException e) {
                    uploadMessage = null;
                    Toast.makeText(getApplicationContext(), "Cannot Open File Chooser", Toast.LENGTH_LONG).show();
                    return false;
                }
                return true;
            }



            @Override
            public void onProgressChanged(WebView view, int newProgress) {
                showLoader();
                if (newProgress == 100)
                    hideLoader();
            }

            @TargetApi(Build.VERSION_CODES.LOLLIPOP)
            @Override
            public void onPermissionRequest(final PermissionRequest request) {
//                runOnUiThread(() -> {
//                    myRequest = request;
//                    if (request.getResources()[0].equals("android.webkit.resource.AUDIO_CAPTURE")) {
//                        checkPermission();
////                    askForPermission(Manifest.permission.RECORD_AUDIO ,MY_PERMISSIONS_REQUEST_RECORD_AUDIO);
//                    } else {
                        request.grant(request.getResources());
//                    }
//                });
            }
        });
        CookieManager.getInstance().setAcceptCookie(true);
        if (android.os.Build.VERSION.SDK_INT >= 21) {
            CookieManager.getInstance().setAcceptThirdPartyCookies(webView, true);
        }
        webView.setWebViewClient(new WebViewClient());
        webView.getSettings().setJavaScriptEnabled(true);
        webView.getSettings().setUseWideViewPort(true);
        webView.getSettings().setLoadWithOverviewMode(true);
        webView.getSettings().supportMultipleWindows();
        webView.getSettings().setDomStorageEnabled(true);
        webView.getSettings().setSupportMultipleWindows(true);
        webView.getSettings().setAllowContentAccess(true);
        webView.getSettings().setAppCacheEnabled(true);
        webView.getSettings().setAllowFileAccess(true);
        webView.getSettings().setAllowFileAccessFromFileURLs(true);
        webView.getSettings().setDatabaseEnabled(true);
        webView.getSettings().setSupportZoom(true);
        webView.getSettings().setDisplayZoomControls(true);
        webView.getSettings().setSaveFormData(true);
        webView.getSettings().setJavaScriptCanOpenWindowsAutomatically(true);
        webView.getSettings().setBuiltInZoomControls(true);
        webView.getSettings().setUserAgentString(DESKTOP_USER_AGENT);
        webView.getSettings().setMediaPlaybackRequiresUserGesture(false);
        webView.setHorizontalFadingEdgeEnabled(true);
        webView.setHorizontalScrollBarEnabled(true);
        webView.setSaveEnabled(true);
        webView.setSaveFromParentEnabled(true);
        webView.setScrollbarFadingEnabled(true);
        webView.setSoundEffectsEnabled(true);
        webView.setHorizontalScrollBarEnabled(true);
        webView.setHorizontalScrollBarEnabled(true);
        webView.setVerticalScrollBarEnabled(true);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            webView.setNestedScrollingEnabled(true);
        }

        webView.setDownloadListener((url, userAgent, contentDisposition, mimeType, contentLength) -> {
            downloadUrl = url;
            downloadUserAgent = userAgent;
            downloadContentDisposition = contentDisposition;
            downloadMimeType = mimeType;
            if (!hasPermissions(this, Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE)) {
                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE,Manifest.permission.READ_EXTERNAL_STORAGE}, 111);
            } else {
                startDownload(url, userAgent, contentDisposition, mimeType);
            }
        });
    }

    private void startDownload(String url, String userAgent, String contentDisposition, String mimeType) {
        try {
            DownloadManager.Request request = new DownloadManager.Request(Uri.parse(url));
            request.setMimeType(mimeType);
            String cookies = CookieManager.getInstance().getCookie(url);
            request.addRequestHeader("cookie", cookies);
            request.addRequestHeader("User-Agent", userAgent);
            request.setDescription("Downloading file...");
            request.setTitle(URLUtil.guessFileName(url, contentDisposition, mimeType));
            request.allowScanningByMediaScanner();
            request.setNotificationVisibility(DownloadManager.Request.VISIBILITY_VISIBLE_NOTIFY_COMPLETED);
            request.setDestinationInExternalPublicDir(
                    Environment.DIRECTORY_DOWNLOADS, URLUtil.guessFileName(
                            url, contentDisposition, mimeType));
            DownloadManager dm = (DownloadManager) getSystemService(DOWNLOAD_SERVICE);
            dm.enqueue(request);
//            onComplete = new BroadcastReceiver() {
//                public void onReceive(Context ctxt, Intent intent) {
//                    try {
//                        Intent intent2 = new Intent(Intent.ACTION_VIEW, dm.getUriForDownloadedFile(ref));
//                        startActivity(intent2);
//                    } catch (Exception ex) {
//                        FirebaseCrashlytics.getInstance().setCustomKey("Open_File", ex.getMessage());
//                        ex.printStackTrace();
//                    }
//                }
//            };
//            registerReceiver(onComplete, new IntentFilter(DownloadManager.ACTION_DOWNLOAD_COMPLETE));
            Toast.makeText(getApplicationContext(), "Downloading File",
                    Toast.LENGTH_LONG).show();
        }catch (Exception e){
            FirebaseCrashlytics.getInstance().setCustomKey("download_file", e.getMessage());
            e.printStackTrace();
            Toast.makeText(getApplicationContext(), "Can't download this file",
                    Toast.LENGTH_LONG).show();
        }
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {
            case R.id.nav_language: {
                if (SharedPref.INSTANCE.getPrefLanguage(this).equalsIgnoreCase("ar")) {
                    SharedPref.INSTANCE.setPrefLanguage(this, "en");
                } else {
                    SharedPref.INSTANCE.setPrefLanguage(this, "ar");
                }
                startActivity(new Intent(this, HomeActivity.class));
                finish();
                item.setChecked(true);
                return true;
            }
            case R.id.nav_home: {
                navHome.setChecked(true);
                bottomHome.setChecked(true);
                homeLayout.setVisibility(View.VISIBLE);
                webviewLayout.setVisibility(View.GONE);
                urlLinearLayout.setVisibility(View.GONE);
                roomsRecyclerView.setVisibility(View.GONE);
                noDataView.setVisibility(View.GONE);
                loadNewsAndCounters();
                drawer.close();
                return true;
            }
            case R.id.nav_tasks: {
                homeLayout.setVisibility(View.GONE);
                webviewLayout.setVisibility(View.VISIBLE);
                urlLinearLayout.setVisibility(View.GONE);
                roomsRecyclerView.setVisibility(View.GONE);
                noDataView.setVisibility(View.GONE);
                bottomMyTasks.setChecked(true);
                if (SharedPref.INSTANCE.getUserType(this).equalsIgnoreCase("student")){
                    webView.loadUrl(buildUrl("MyLMS/MyTasks.aspx"));
                }else if (SharedPref.INSTANCE.getUserType(this).equalsIgnoreCase("teacher")){
                    webView.loadUrl(buildUrl("LMSTeacher/LMS_Tasks.aspx"));
                }

                drawer.close();
                return true;
            }
            case R.id.nav_class_room: {
                homeLayout.setVisibility(View.GONE);
                webviewLayout.setVisibility(View.GONE);
                urlLinearLayout.setVisibility(View.GONE);
                roomsRecyclerView.setVisibility(View.GONE);
                noDataView.setVisibility(View.GONE);
                bottomClassRoom.setChecked(true);
                getClassRoomLink();
                drawer.close();
                return true;
            }
            case R.id.nav_schedule: {
                homeLayout.setVisibility(View.GONE);
                webviewLayout.setVisibility(View.VISIBLE);
                urlLinearLayout.setVisibility(View.GONE);
                roomsRecyclerView.setVisibility(View.GONE);
                bottomSchedule.setChecked(true);
//                webView.loadUrl("https://zoom.us/wc/join/9047488056?pwd=123");
                webView.loadUrl(buildUrl("MyLMS/Schedule.aspx"));
                drawer.close();
                return true;
            }
            case R.id.nav_setting: {
                homeLayout.setVisibility(View.GONE);
                webviewLayout.setVisibility(View.VISIBLE);
                urlLinearLayout.setVisibility(View.GONE);
                roomsRecyclerView.setVisibility(View.GONE);
                noDataView.setVisibility(View.GONE);
                item.setChecked(true);
                drawer.close();
                return true;
            }
            case R.id.nav_about: {
                homeLayout.setVisibility(View.GONE);
                webviewLayout.setVisibility(View.VISIBLE);
                urlLinearLayout.setVisibility(View.GONE);
                roomsRecyclerView.setVisibility(View.GONE);
                noDataView.setVisibility(View.GONE);
                item.setChecked(true);
                drawer.close();
                return true;
            }
            case R.id.logout: {
                String FCMToken = SharedPref.INSTANCE.getFCMToken(this);
                SharedPref.INSTANCE.clearAll(HomeActivity.this);
                SharedPref.INSTANCE.setFCMToken(this, FCMToken);
                Intent intent = new Intent(this, SplashActivity.class);
                startActivity(intent);
                finish();
                return true;
            }
            default:
                return false;
        }
    }

    private void getClassRoomLink() {
        showLoader();
        NetworkService.getInstance().VirtualClassRoom(new BaseResponse.BaseResponseListener<ClassRoomResponse>() {
            @Override
            public void onFailed(String errorMessage) {
                hideLoader();
                homeLayout.setVisibility(View.GONE);
                webviewLayout.setVisibility(View.GONE);
                urlLinearLayout.setVisibility(View.GONE);
                roomsRecyclerView.setVisibility(View.GONE);
                noDataView.setVisibility(View.VISIBLE);
            }

            @Override
            public void onSuccess(ClassRoomResponse body) {
                hideLoader();
                rooms.clear();
                rooms.addAll(body.getTable());
                classRoomAdapter.notifyDataSetChanged();
                homeLayout.setVisibility(View.GONE);
                webviewLayout.setVisibility(View.GONE);
                urlLinearLayout.setVisibility(View.GONE);
                roomsRecyclerView.setVisibility(View.VISIBLE);
                noDataView.setVisibility(View.GONE);
            }
        }, this, SharedPref.INSTANCE.getUserToken(this));
    }

    @Override
    public void onNavigationItemReselected(@NonNull MenuItem item) {
        switch (item.getItemId()){
            case R.id.nav_home: {
                bottomHome.setChecked(true);
                homeLayout.setVisibility(View.VISIBLE);
                webviewLayout.setVisibility(View.GONE);
                urlLinearLayout.setVisibility(View.GONE);
                roomsRecyclerView.setVisibility(View.GONE);
                noDataView.setVisibility(View.GONE);
                loadNewsAndCounters();
                drawer.close();
                break;
            }
            case R.id.nav_tasks: {
                homeLayout.setVisibility(View.GONE);
                webviewLayout.setVisibility(View.VISIBLE);
                urlLinearLayout.setVisibility(View.GONE);
                roomsRecyclerView.setVisibility(View.GONE);
                noDataView.setVisibility(View.GONE);
                bottomMyTasks.setChecked(true);
                if (SharedPref.INSTANCE.getUserType(this).equalsIgnoreCase("student")){
                    webView.loadUrl(buildUrl("MyLMS/MyTasks.aspx"));
                }else if (SharedPref.INSTANCE.getUserType(this).equalsIgnoreCase("teacher")){
                    webView.loadUrl(buildUrl("LMSTeacher/LMS_Tasks.aspx"));
                }
                drawer.close();
                break;
            }
            case R.id.nav_class_room: {
                homeLayout.setVisibility(View.GONE);
                webviewLayout.setVisibility(View.GONE);
                urlLinearLayout.setVisibility(View.GONE);
                roomsRecyclerView.setVisibility(View.GONE);
                noDataView.setVisibility(View.GONE);
                bottomClassRoom.setChecked(true);
                getClassRoomLink();
                drawer.close();
                break;
            }
            case R.id.nav_schedule: {
                homeLayout.setVisibility(View.GONE);
                webviewLayout.setVisibility(View.VISIBLE);
                urlLinearLayout.setVisibility(View.GONE);
                roomsRecyclerView.setVisibility(View.GONE);
                bottomSchedule.setChecked(true);
                webView.loadUrl(buildUrl("MyLMS/Schedule.aspx"));
                drawer.close();
                break;
            }
        }
    }

    @Override
    public void onBackPressed() {
        if (webviewLayout.getVisibility() == View.VISIBLE) {
            if (webView.canGoBack())
                webView.goBack();
            else{
                navHome.setChecked(true);
                bottomHome.setChecked(true);
                homeLayout.setVisibility(View.VISIBLE);
                webviewLayout.setVisibility(View.GONE);
                urlLinearLayout.setVisibility(View.GONE);
                roomsRecyclerView.setVisibility(View.GONE);
                noDataView.setVisibility(View.GONE);
            }
        } else {
            super.onBackPressed();
        }
    }

    private void checkPermission() {
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.RECORD_AUDIO) != PackageManager.PERMISSION_GRANTED || ContextCompat.checkSelfPermission(this, Manifest.permission.MODIFY_AUDIO_SETTINGS) != PackageManager.PERMISSION_GRANTED || ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.RECORD_AUDIO, Manifest.permission.MODIFY_AUDIO_SETTINGS, Manifest.permission.CAMERA}, 1);
        } else {

        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           @NotNull String permissions[], @NotNull int[] grantResults) {
        switch (requestCode) {
            case 111:{
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    startDownload(downloadUrl, downloadUserAgent, downloadContentDisposition, downloadMimeType);
                }else{

                }
                break;
            }
            case MY_PERMISSIONS_REQUEST_RECORD_AUDIO: {
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    myRequest.grant(myRequest.getResources());
                } else {

                }
                break;
            }
            case MY_PERMISSIONS_REQUEST_CAMERA: {
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    myRequest.grant(myRequest.getResources());
                } else {

                }
                break;
            }
        }
    }

    public void askForPermission(String permission, int requestCode) {

        if (ContextCompat.checkSelfPermission(this,
                permission)
                != PackageManager.PERMISSION_GRANTED) {
            if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                    permission)) {
            } else {
                ActivityCompat.requestPermissions(this,
                        new String[]{permission},
                        requestCode);
            }
        } else {
            myRequest.grant(myRequest.getResources());
        }
    }

    public void animateCounter(final TextView view, int counter) {
        ValueAnimator animator = new ValueAnimator();
        animator.setObjectValues(0, counter);
        animator.addUpdateListener(animation -> view.setText(String.valueOf(animation.getAnimatedValue())));
        if (counter <= 10)
            animator.setDuration(counter * 200);
        else if (counter <= 20)
            animator.setDuration(counter * 150);
        else
            animator.setDuration(counter * 75);
        animator.start();
    }

    String buildUrl(String path) {
        return SharedPref.INSTANCE.getERPUrl(this) + "Login.aspx?username="
                + SharedPref.INSTANCE.getUserEmail(this) + "&usertoken="
                + SharedPref.INSTANCE.getUserToken(this) + "&url="
                + SharedPref.INSTANCE.getERPUrl(this) + path;
    }

    @Override
    public void onCLick(@NotNull ClassRoomModel room) {
        homeLayout.setVisibility(View.GONE);
        webviewLayout.setVisibility(View.VISIBLE);
        urlLinearLayout.setVisibility(View.GONE);
        roomsRecyclerView.setVisibility(View.GONE);
        noDataView.setVisibility(View.GONE);
        webView.loadUrl(room.getVirtualClassRoomLink());
    }



    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            if (requestCode == REQUEST_SELECT_FILE) {
                if (uploadMessage == null)
                    return;
                uploadMessage.onReceiveValue(WebChromeClient.FileChooserParams.parseResult(resultCode, data));
                uploadMessage = null;
            }
        } else if (requestCode == FILECHOOSER_RESULTCODE) {
            if (null == mUploadMessage)
                return;
            // Use MainActivity.RESULT_OK if you're implementing WebView inside Fragment
            // Use RESULT_OK only if you're implementing WebView inside an Activity
            Uri result = data == null || resultCode != HomeActivity.RESULT_OK ? null : data.getData();
            mUploadMessage.onReceiveValue(result);
            mUploadMessage = null;
        } else
            Toast.makeText(this, R.string.failed_to_upload, Toast.LENGTH_LONG).show();
    }

    @Override
    public void onItemClick(@NotNull NewsModel news) {
        homeLayout.setVisibility(View.GONE);
        webviewLayout.setVisibility(View.VISIBLE);
        urlLinearLayout.setVisibility(View.GONE);
        roomsRecyclerView.setVisibility(View.GONE);
        noDataView.setVisibility(View.GONE);
        webView.loadUrl(SharedPref.INSTANCE.getERPUrl(this) + "details.aspx?News_Id=" + news.getId() + "&username=" + SharedPref.INSTANCE.getUserEmail(this) + "&usertoken=" + SharedPref.INSTANCE.getUserToken(this));
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (onComplete != null)
            unregisterReceiver(onComplete);
    }
}
