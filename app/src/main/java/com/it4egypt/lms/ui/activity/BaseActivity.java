package com.it4egypt.lms.ui.activity;

import android.content.res.Configuration;
import android.os.Bundle;
import android.view.View;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.it4egypt.lms.SharedPref;
import com.kaopiz.kprogresshud.KProgressHUD;

import java.util.Locale;

public class BaseActivity extends AppCompatActivity {
    private KProgressHUD dialog;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setLocale(SharedPref.INSTANCE.getPrefLanguage(this));
    }

    public void setLocale(String lang) {
        if(lang.equals("en")) {
            getWindow().getDecorView().setLayoutDirection(View.LAYOUT_DIRECTION_LTR);
            Configuration con = getResources().getConfiguration();
            con.setLayoutDirection(new Locale("en"));
            con.setLocale(new Locale("en"));
            getResources().updateConfiguration(con,getResources().getDisplayMetrics());
        }else{
            getWindow().getDecorView().setLayoutDirection(View.LAYOUT_DIRECTION_RTL);
            Configuration con = getResources().getConfiguration();
            con.setLayoutDirection(new Locale("ar"));
            con.setLocale(new Locale("ar"));
            getResources().updateConfiguration(con,getResources().getDisplayMetrics());
        }

    }

    void showLoader(){
        if(dialog == null) {
            dialog = KProgressHUD.create(this)
                    .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
                    .setCancellable(false)
                    .setAnimationSpeed(2)
                    .setDimAmount(0.5f)
                    .show();
        }else{
            dialog.show();
        }
    }

    void hideLoader(){
        dialog.dismiss();
    }
}
