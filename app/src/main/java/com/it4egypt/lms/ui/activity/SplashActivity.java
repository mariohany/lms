package com.it4egypt.lms.ui.activity;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;

import androidx.appcompat.app.AlertDialog;

import com.it4egypt.lms.BuildConfig;
import com.it4egypt.lms.LMSApplication;
import com.it4egypt.lms.R;
import com.it4egypt.lms.SharedPref;
import com.it4egypt.lms.retrofit.BaseResponse;
import com.it4egypt.lms.retrofit.NetworkService;

public class SplashActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
//        ImageView imageView = findViewById(R.id.imageView);
//        if (SharedPref.INSTANCE.getUserType(this).equalsIgnoreCase("student")){
//            imageView.setImageResource(R.drawable.logo_student_white);
//            imageView.setLayoutParams(params);
//        }else{
//            imageView.setImageResource(R.drawable.logo_white);
//        }
        setLocale(SharedPref.INSTANCE.getPrefLanguage(this));
        if(getIntent().hasExtra("redirectUrl")){
            if(getIntent().getStringExtra("redirectUrl") != null && !getIntent().getStringExtra("redirectUrl").isEmpty()) {
                LMSApplication.Companion.setUrl(getIntent().getStringExtra("redirectUrl"));
            }
        }
        new Handler().postDelayed(() -> internetAvailability(),1500);
    }

    void internetAvailability(){
        if (isNetworkAvailable())
            checkVersion();
        else
            displayNetworkAlert(SplashActivity.this, (dialog, which) -> {
                internetAvailability();
            });
    }

    void checkVersion(){
        NetworkService.getInstance().CheckVersion(new BaseResponse.BaseResponseListener<Integer>() {
            @Override
            public void onFailed(String errorMessage) {
                displayAlertErr(SplashActivity.this, getString(R.string.error_msg), (dialog, which) -> {
                    dialog.dismiss();
                    checkVersion();
                });
            }

            @Override
            public void onSuccess(Integer body) {
                if(BuildConfig.VERSION_CODE < body){
                    String FCMToken = SharedPref.INSTANCE.getFCMToken(SplashActivity.this);
                    SharedPref.INSTANCE.clearAll(SplashActivity.this);
                    SharedPref.INSTANCE.setFCMToken(SplashActivity.this, FCMToken);
                    displayAlertUpdate(SplashActivity.this, getString(R.string.update_message), (dialog, which) -> {
                        final String appPackageName = getPackageName();
                        try {
                            startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + appPackageName)));
                        } catch (android.content.ActivityNotFoundException anfe) {
                            startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + appPackageName)));
                        }
                    });
                }else{
                    if (SharedPref.INSTANCE.getUserToken(SplashActivity.this) == null) {
                        startActivity(new Intent(SplashActivity.this, LoginActivity.class));
                        finish();
                    }else{
                        startActivity(new Intent(SplashActivity.this, HomeActivity.class));
                        finish();
                    }
                }
            }
        }, this);
    }

    void displayAlertUpdate(Context context, String msg, DialogInterface.OnClickListener listener){
        if (context != null) {
            AlertDialog.Builder builder = new AlertDialog.Builder(context, R.style.DialogeTheme);
            builder.setTitle("My LMS");
            builder.setMessage(msg);
            builder.setPositiveButton(R.string.update, listener);
            AlertDialog dialog = builder.create();
            dialog.setCancelable(false);
            dialog.setCanceledOnTouchOutside(false);
            dialog.show();
        }
    }

    void displayAlertErr(Context context, String msg, DialogInterface.OnClickListener listener){
        if (context != null) {
            AlertDialog.Builder builder = new AlertDialog.Builder(context, R.style.DialogeTheme);
            builder.setTitle("My LMS");
            builder.setMessage(msg);
            builder.setPositiveButton(R.string.ok, listener);
            AlertDialog dialog = builder.create();
            dialog.setCancelable(false);
            dialog.setCanceledOnTouchOutside(false);
            dialog.show();
        }
    }

    void displayNetworkAlert(Context context, DialogInterface.OnClickListener listener){
        if (context != null) {
            AlertDialog.Builder builder = new AlertDialog.Builder(context, R.style.DialogeTheme);
            builder.setTitle("My LMS");
            builder.setMessage(context.getString(R.string.no_internet));
            builder.setPositiveButton(R.string.ok, listener);
            AlertDialog dialog = builder.create();
            dialog.setCancelable(false);
            dialog.setCanceledOnTouchOutside(false);
            dialog.show();
        }
    }

    private boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }
}