package com.it4egypt.lms.ui.activity

import android.content.Intent
import android.os.Bundle
import android.widget.Button
import android.widget.EditText
import android.widget.Toast
import com.it4egypt.lms.R
import com.it4egypt.lms.SharedPref
import com.it4egypt.lms.SharedPref.getPrefLanguage
import com.it4egypt.lms.models.DeviceData
import com.it4egypt.lms.models.LoginResponse
import com.it4egypt.lms.models.School
import com.it4egypt.lms.retrofit.BaseResponse.BaseResponseListener
import com.it4egypt.lms.retrofit.NetworkService
import com.jaredrummler.android.device.DeviceName
import java.util.*

class LoginActivity : BaseActivity() {
    private lateinit var login: Button
    private lateinit var email: EditText
    private lateinit var pass: EditText
    private lateinit var code: EditText

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)
//        window.setFlags(WindowManager.LayoutParams.FLAG_SECURE, WindowManager.LayoutParams.FLAG_SECURE)
        login = findViewById(R.id.login)
        email = findViewById(R.id.email)
        pass = findViewById(R.id.password)
        code = findViewById(R.id.code)
        DeviceName.init(this);
        login.setOnClickListener {
            if (email.text.isNotEmpty()) {
                if (pass.text.isNotEmpty()) {
                    if (code.text.isNotEmpty()) {
                        showLoader()
                        NetworkService.getInstance().GetSchoolConfig(object : BaseResponseListener<ArrayList<School>> {
                            override fun onFailed(errorMessage: String?) {
                                hideLoader()
                                Toast.makeText(this@LoginActivity, errorMessage, Toast.LENGTH_SHORT).show()
                            }

                            override fun onSuccess(body: ArrayList<School>) {
                                SharedPref.setERPUrl(this@LoginActivity, body[0].ERPUrl)
                                SharedPref.setAPIUrl(this@LoginActivity, body[0].APIUrl)
                                val model = DeviceData(SharedPref.getFCMToken(this@LoginActivity), "Android", DeviceName.getDeviceName(), android.os.Build.VERSION.SDK_INT.toString())
                                NetworkService.getInstance().Login(object : BaseResponseListener<LoginResponse> {
                                    override fun onFailed(errorMessage: String?) {
                                        hideLoader()
                                        Toast.makeText(this@LoginActivity, errorMessage, Toast.LENGTH_SHORT).show()
                                    }

                                    override fun onSuccess(body: LoginResponse) {
                                        SharedPref.setUserId(this@LoginActivity, body.UserId)
                                        SharedPref.setUserType(this@LoginActivity, body.UserType)
                                        SharedPref.setUserName(this@LoginActivity, body.Fullname)
                                        NetworkService.getInstance().FirebaseToken(object : BaseResponseListener<String> {
                                            override fun onFailed(errorMessage: String?) {
                                                hideLoader()
                                                Toast.makeText(this@LoginActivity, errorMessage, Toast.LENGTH_SHORT).show()
                                            }

                                            override fun onSuccess(body: String) {
                                                SharedPref.setUserToken(this@LoginActivity, body)
                                                SharedPref.setUserEmail(this@LoginActivity, email.text.toString())
                                                SharedPref.setUserPassword(this@LoginActivity, pass.text.toString())
                                                startActivity(Intent(this@LoginActivity, HomeActivity::class.java))
                                                hideLoader()
                                                finish()
                                            }

                                        }, this@LoginActivity, body.UserId, model)
                                    }

                                }, this@LoginActivity, email.text.toString(), pass.text.toString())
                            }

                        }, this@LoginActivity, code.text.toString())
                    }else
                        code.error = getString(R.string.empty_validation)
                }else
                    pass.error = getString(R.string.empty_validation)
            }else
                email.error = getString(R.string.empty_validation)
        }

        setLocale(getPrefLanguage(this))
    }


}