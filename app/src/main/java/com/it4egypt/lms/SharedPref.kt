package com.it4egypt.lms

import android.content.Context
import android.content.SharedPreferences

object SharedPref {
    private const val PREF_KEY = "LMS_PREF_KEY"
    private const val FCM_TOKEN = "FCM_TOKEN"
    private const val PREF_LANGUAGE = "PREF_LANGUAGE"
    private const val PREF_EMAIL = "PREF_EMAIL"
    private const val PREF_USER_ID = "PREF_USER_ID"
    private const val PREF_USER_TYPE = "PREF_USER_TYPE"
    private const val PREF_PASSWORD = "PREF_PASSWORD"
    private const val PREF_API_URL = "PREF_API_URL"
    private const val PREF_ERP_URL = "PREF_ERP_URL"
    private const val PREF_TOKEN = "PREF_TOKEN"
    private const val PREF_NAME = "PREF_NAME"

    fun setFCMToken(context: Context, token: String) {
        val sharedPref: SharedPreferences.Editor = context.getSharedPreferences(PREF_KEY,Context.MODE_PRIVATE).edit()
        sharedPref.putString(FCM_TOKEN,token)
        sharedPref.apply()
    }

    fun getFCMToken(context: Context): String? {
        val sharedPref:SharedPreferences = context.getSharedPreferences(PREF_KEY,Context.MODE_PRIVATE)
        return sharedPref.getString(FCM_TOKEN, null)
    }

    fun setPrefLanguage(context: Context, language: String) {
        val sharedPref: SharedPreferences.Editor = context.getSharedPreferences(PREF_KEY,Context.MODE_PRIVATE).edit()
        sharedPref.putString(PREF_LANGUAGE, language)
        sharedPref.apply()
    }

    fun getPrefLanguage(context: Context): String? {
        val sharedPref:SharedPreferences = context.getSharedPreferences(PREF_KEY,Context.MODE_PRIVATE)
        return sharedPref.getString(PREF_LANGUAGE, "en")
    }

    fun setUserEmail(context: Context, language: String) {
        val sharedPref: SharedPreferences.Editor = context.getSharedPreferences(PREF_KEY,Context.MODE_PRIVATE).edit()
        sharedPref.putString(PREF_EMAIL, language)
        sharedPref.apply()
    }

    fun getUserEmail(context: Context): String? {
        val sharedPref:SharedPreferences = context.getSharedPreferences(PREF_KEY,Context.MODE_PRIVATE)
        return sharedPref.getString(PREF_EMAIL, null)
    }

    fun setUserName(context: Context, language: String) {
        val sharedPref: SharedPreferences.Editor = context.getSharedPreferences(PREF_KEY,Context.MODE_PRIVATE).edit()
        sharedPref.putString(PREF_NAME, language)
        sharedPref.apply()
    }

    fun getUserName(context: Context): String? {
        val sharedPref:SharedPreferences = context.getSharedPreferences(PREF_KEY,Context.MODE_PRIVATE)
        return sharedPref.getString(PREF_NAME, null)
    }

    fun setUserId(context: Context, id: Int) {
        val sharedPref: SharedPreferences.Editor = context.getSharedPreferences(PREF_KEY,Context.MODE_PRIVATE).edit()
        sharedPref.putInt(PREF_USER_ID, id)
        sharedPref.apply()
    }

    fun getUserId(context: Context): Int {
        val sharedPref:SharedPreferences = context.getSharedPreferences(PREF_KEY,Context.MODE_PRIVATE)
        return sharedPref.getInt(PREF_USER_ID, 0)
    }

    fun setUserType(context: Context, type: String) {
        val sharedPref: SharedPreferences.Editor = context.getSharedPreferences(PREF_KEY,Context.MODE_PRIVATE).edit()
        sharedPref.putString(PREF_USER_TYPE, type)
        sharedPref.apply()
    }

    fun getUserType(context: Context): String? {
        val sharedPref:SharedPreferences = context.getSharedPreferences(PREF_KEY,Context.MODE_PRIVATE)
        return sharedPref.getString(PREF_USER_TYPE, "student")
    }

    fun setUserPassword(context: Context, language: String) {
        val sharedPref: SharedPreferences.Editor = context.getSharedPreferences(PREF_KEY,Context.MODE_PRIVATE).edit()
        sharedPref.putString(PREF_PASSWORD, language)
        sharedPref.apply()
    }

    fun getUserPassword(context: Context): String? {
        val sharedPref:SharedPreferences = context.getSharedPreferences(PREF_KEY,Context.MODE_PRIVATE)
        return sharedPref.getString(PREF_PASSWORD, null)
    }

    fun setAPIUrl(context: Context, url: String) {
        val sharedPref: SharedPreferences.Editor = context.getSharedPreferences(PREF_KEY,Context.MODE_PRIVATE).edit()
        sharedPref.putString(PREF_API_URL, url)
        sharedPref.apply()
    }

    fun getAPIUrl(context: Context): String {
        val sharedPref:SharedPreferences = context.getSharedPreferences(PREF_KEY,Context.MODE_PRIVATE)
        return sharedPref.getString(PREF_API_URL, "")?:""
    }

    fun setERPUrl(context: Context, url: String) {
        val sharedPref: SharedPreferences.Editor = context.getSharedPreferences(PREF_KEY,Context.MODE_PRIVATE).edit()
        sharedPref.putString(PREF_ERP_URL, url)
        sharedPref.apply()
    }

    fun getERPUrl(context: Context): String? {
        val sharedPref:SharedPreferences = context.getSharedPreferences(PREF_KEY,Context.MODE_PRIVATE)
        return sharedPref.getString(PREF_ERP_URL, null)
    }

    fun setUserToken(context: Context, token: String) {
        val sharedPref: SharedPreferences.Editor = context.getSharedPreferences(PREF_KEY,Context.MODE_PRIVATE).edit()
        sharedPref.putString(PREF_TOKEN, token)
        sharedPref.apply()
    }

    fun getUserToken(context: Context): String? {
        val sharedPref:SharedPreferences = context.getSharedPreferences(PREF_KEY,Context.MODE_PRIVATE)
        return sharedPref.getString(PREF_TOKEN, null)
    }

    fun clearAll(context: Context) {
        val sharedPref:SharedPreferences.Editor = context.getSharedPreferences(PREF_KEY,Context.MODE_PRIVATE).edit()
        sharedPref.clear()
        sharedPref.apply()
    }
}