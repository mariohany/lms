package com.it4egypt.lms.models

import java.io.Serializable

data class ClassRoomResponse (
        val Table: ArrayList<ClassRoomModel>
):Serializable