package com.it4egypt.lms.models

import java.io.Serializable

data class MenuItem (
        val Arabic: String,
        val English: String,
        val NavigateUrl:String,
        val Icon:String
): Serializable