package com.it4egypt.lms.models

import com.google.gson.annotations.SerializedName
import java.io.Serializable

data class ClassRoomModel (
        @SerializedName("virtual_classromm_link") val virtualClassRoomLink: String,
        @SerializedName("ZoomAppWebLink") val zoomAppWebLink: String,
        @SerializedName("ZoomAppMobileLink") val zoomAppMobileLink: String,
        @SerializedName("MeetingId") val meetingId: String,
        @SerializedName("MeetingPassword") val meetingPassword: String,
        @SerializedName("meeting_from_time") val from: String,
        @SerializedName("meeting_to_time") val to: String,
        @SerializedName("subject_en") val subjectEn: String,
        @SerializedName("subject_ar") val subjectAr: String,
        @SerializedName("arabic") val teacherAr: String,
        @SerializedName("english") val teacherEn: String
):Serializable