package com.it4egypt.lms.models

import com.google.gson.annotations.SerializedName
import java.io.Serializable

data class CountersResponse (
        @SerializedName("Task_Homework")
        val homework: Int,
        @SerializedName("QuizOnline")
        val onlineQuiz: Int,
        @SerializedName("QuizHandon")
        val handonQuiz: Int,
        @SerializedName("ElearningLesson")
        val Lessons: Int
):Serializable