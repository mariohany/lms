package com.it4egypt.lms.models

import com.google.gson.annotations.SerializedName
import java.io.Serializable

data class DeviceData (
        @SerializedName("token_no")
        val token: String?,
        @SerializedName("Device_type")
        private val type: String? = "Android",
        @SerializedName("Device_mode")
        val model: String?,
        @SerializedName("Device_os_ver")
        val androidVersion: String?
):Serializable