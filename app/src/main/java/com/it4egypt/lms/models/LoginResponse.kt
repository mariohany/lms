package com.it4egypt.lms.models

import java.io.Serializable

data class LoginResponse(
        val UserId: Int,
        val UserType: String,
        val Fullname: String
):Serializable