package com.it4egypt.lms.models

import java.io.Serializable

data class School (
        val ERPUrl:String,
        val APIUrl:String
): Serializable