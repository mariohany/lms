package com.it4egypt.lms.models

import com.google.gson.annotations.SerializedName
import java.io.Serializable

data class NewsModel (
        @SerializedName("News_Id")
        val id: Int,
        @SerializedName("Title")
        val title: String,
        @SerializedName("OrderBy")
        val order: Int,
        @SerializedName("date_insert_db")
        val insertDate: String,
        @SerializedName("ImageUrl")
        val imageUrl: String,
        @SerializedName("Body")
        val body: String,
        @SerializedName("ValidFromDate")
        val validFromDate: String?,
):Serializable