package com.it4egypt.lms

import android.content.Context
import android.content.Intent
import android.net.Uri
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.it4egypt.lms.models.ClassRoomModel

class ClassRoomAdapter(val context: Context, val rooms: MutableList<ClassRoomModel>, val listener: RoomItemClick): RecyclerView.Adapter<ClassRoomAdapter.ViewHolder>() {


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val itemView: View = LayoutInflater.from(context).inflate(R.layout.room_list_item, parent, false)
        return ViewHolder(itemView)
    }

    override fun getItemCount(): Int {
        return rooms.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(context, rooms[position])
        holder.hereBtn.setOnClickListener {
            listener.onCLick(rooms[position])
        }

        holder.zoomBtn.setOnClickListener {
            val intent = Intent(Intent.ACTION_VIEW, Uri.parse(rooms[position].zoomAppMobileLink))
            context.startActivity(intent)
        }
    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val teacher: TextView = itemView.findViewById(R.id.teacher)
        val meetingId: TextView = itemView.findViewById(R.id.meeting_id)
        val meetingPass: TextView = itemView.findViewById(R.id.meeting_pass)
        val from: TextView = itemView.findViewById(R.id.from)
        val to: TextView = itemView.findViewById(R.id.to)
        val subject: TextView = itemView.findViewById(R.id.subject)
        val hereBtn: Button = itemView.findViewById(R.id.here_btn)
        val zoomBtn: Button = itemView.findViewById(R.id.zoom_btn)

        fun bind(context: Context, room: ClassRoomModel) {
            if (SharedPref.getPrefLanguage(context).equals("ar", true)){
                teacher.text = room.teacherAr
                subject.text = room.subjectAr
            }else{
                teacher.text = room.teacherEn
                subject.text = room.subjectEn
            }
            meetingId.text = room.meetingId
            meetingPass.text = room.meetingPassword
            from.text = room.from
            to.text = room.to
        }

    }
}